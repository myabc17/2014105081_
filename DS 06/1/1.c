#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_SIZE 100
#define TRUE 1
#define FALSE 0

typedef struct {
	short int row;
	short int col;
	short int dir;
}element;
typedef struct {
	short int vert;
	short int horiz;
} offsets;
offsets move[8];
element stack[MAX_STACK_SIZE];
int top = -1;
int EXIT_ROW;
int EXIT_COL;
int mark[10][10];
int maze[10][10];

void push(element temp)
{
	stack[++top] = temp;
}

element pop()
{
	return stack[top--];
}
void path(void)
{
	int i, row, col, nextRow, nextCol, dir, found = FALSE;
	element position;
	mark[1][1] = 1; top = 0;
	stack[0].row = 1; stack[0].col = 1; stack[0], dir = 1;
	while (top > -1 && !found) {
		position = pop();
		row = position.row; col = position.col;
		dir = position.dir;
		while (dir < 8 && !found) {
			nextRow = row + move[dir].vert;
			nextCol = col + move[dir].horiz;
			if (nextRow == EXIT_ROW && nextCol == EXIT_COL)
				found = TRUE;
			else if (!(maze[nextRow][nextCol]) && !(mark[nextRow][nextCol])) {
				mark[nextRow][nextCol] = 1;
				position.row = row; position.col = col;
				position.dir = ++dir;
				push(position);
				row = nextRow; col = nextCol; dir = 0;
			}
			else ++dir;
		}
	}
	if (found) {
		printf("The path is : \n");
		printf("row col\n");
		for (i = 0; i <= top; i++)
			printf("%2d%5d\n", stack[i].row, stack[i].col);
		printf("%2d%5d\n", row, col);
		printf("%2d%5d\n", EXIT_ROW, EXIT_COL);
	}
	else printf("The maze does not have a path\n");
}

void main()
{
	FILE *f = fopen("input.txt", "r");
	FILE *direction = fopen("dir.txt", "r");
	int i, j;
	fscanf(f, "%d %d", &EXIT_ROW, &EXIT_COL);
	for (i = 0; i < EXIT_ROW+2; i++) {
		for (j = 0; j < EXIT_COL+2; j++) {
			if ((i == 0 || j == 0) || (i == EXIT_ROW + 1 || j == EXIT_COL + 1))
				maze[i][j] = 1;
			else
				fscanf(f, "%d", &maze[i][j]);
		}
	}
	for (i = 0; i < EXIT_ROW + 2; i++) {
		for (j = 0; j < EXIT_COL + 2; j++) {
			printf("%d ", maze[i][j]);
		}
		printf("\n");
	}
	for (i = 0; i < 8; i++) {
		fscanf(direction, "%d %d", &move[i].vert, &move[i].horiz);
	}
	path();
	fclose(f);
	fclose(direction);
}