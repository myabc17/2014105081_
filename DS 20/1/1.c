#include <stdio.h>
#define MAX_SIZE 20

typedef struct element {
	int key;
}element;

FILE *f1, *f2;
element a[MAX_SIZE];

void printFR(int *front, int *rear, int r)
{
	int i;
	printf("\n          ");
	for (i = 0; i < r; i++)
		printf("[%2d] ", i);
	printf("\n%10s", "front:");
	for (i = 0; i < r; i++) {
		printf("%4d ", front[i]);
	}
	printf("\n%10s", "rear:");
	for (i = 0; i < r; i++) {
		printf("%4d ", rear[i]);
	}
	printf("\n");
}
void printList(element a[], int link[], int first, int n)
{
	int i, temp;
	temp = first;
	for (i = 1; i <= n; i++) {
		printf("[%2d] ", i);
	}
	printf("\n%10s", "link:");
	for (i = 1; i <= n; i++)
		printf("%4d ", link[i]);
	printf("\n%10s", "a:");
	for (i = 1; i <= n; i++)
		printf("%4d ", a[i]);
	printf("\n%10s", "first:");
	printf("%4d\n", first);
	printf("\n\n%10s", "result:");
	for (i = 1; i <= n; i++) {
		printf("%4d ", a[temp]);
		temp = link[temp];
	}
	printf("\n");
}
int digit(element a, int i, int r, int d)
{
	int temp, ch, t;
	temp = a.key;
	ch = d - i;
	for (t = 1; t < ch; t++) {
		temp = temp / 10;
	}
	return temp % 10;
}
int radixSort(element a[], int link[], int d, int r, int n)
{
	int *front, *rear;
	int i, bin, current, first, last;
	int count = 1;
	front = (int)malloc(sizeof(*front)*r);
	rear = (int)malloc(sizeof(*rear)*r);
	first = 1;
	for (i = 1; i < n; i++) link[i] = i + 1;
	link[n] = 0;
	printf("****************************** initial chain******************************\n          ");
	printList(a, link, first, n);
	for (i = d - 1; i >= 0; i--)
	{
		
		for (bin = 0; bin < r; bin++) {
			front[bin] = 0;
			rear[bin] = 0;
		}
		for (current = first; current; current = link[current])
		{
			bin = digit(a[current], i, r, d);
			if (front[bin] == 0) front[bin] = current;
			else link[rear[bin]] = current;
			rear[bin] = current;
		}
		for (bin = 0; !front[bin]; bin++);
		first = front[bin]; last = rear[bin];

		for (bin++; bin < r;bin++)
		if (front[bin])
		{
			link[last] = front[bin]; last = rear[bin];
		}
		link[last] = 0;
		printf("\n****************************** pass %d ******************************\n          ", count++);
		printList(a, link, first, n);
		printFR(front, rear, r);
	}
	
	return first;
}

void main()
{
	f1 = fopen("input.txt", "r");
	f2 = fopen("output.txt", "w");
	int first, i = 1, d, n, temp;
	int *link;
	fscanf(f1, "%d %d", &d, &n);
	link = (int)malloc(sizeof(*link)*n);
	while (fscanf(f1, "%d", &temp) != -1) {
		a[i].key = temp;
		i = i + 1;
	}
	first = radixSort(a, link, d, 10, n);
	for (i = 1; i <= n; i++) {
		fprintf(f2, "%3d", a[first]);
		first = link[first];
	}
	fclose(f1);
	fclose(f2);
}