#include <stdio.h>
#include <stdlib.h>
#define MAX_NAME_SIZE 20

typedef struct {
	int no;
	char name[MAX_NAME_SIZE];
}element;
element *queue;

int capacity = 2;
int rear = 0;
int front = 0;

element copy(element *queue1, element *queue2, element *newqueue)
{
	int i;
	for (i = 0; queue1 + i != queue2; i++) {
		(newqueue + i)->no = (queue1 + i)->no;
		strcpy((newqueue + i)->name, (queue1 + i)->name);
	}
}

element queueEmpty()
{
	printf("queue is empty, cannot delete element.\n");
	exit(EXIT_FAILURE);
}

element deleteq()
{
	element item;
	if (front == rear)
		return queueEmpty();
	front = (front + 1) % capacity;
	return queue[front];
}

void queueFull()
{
	int start;
	element* newQueue;
	newQueue = malloc(2 * capacity*sizeof(*queue));

	start = (front + 1) % capacity; rear--;
	if (start < 2)
		copy(queue + start, queue + start + capacity - 1, newQueue);
	else
	{
		copy(queue + start, queue + capacity, newQueue);
		copy(queue, queue + rear + 1, newQueue + capacity - start);
	}

	front = 2 * capacity - 1;
	rear = capacity - 1;
	capacity *= 2;
	printf("queue capacity is doubled.\n");
	printf("current queue capacity is %d\n", capacity);
	free(queue);
	queue = newQueue;
}

void addq(element item)
{
	rear = (rear + 1) % capacity;
	if (front == rear)
		queueFull();
	queue[rear] = item;
}


void main(void)
{
	char input[81];
	char temp[81];
	element te;

	queue = (element *)malloc(sizeof(element) * capacity);
	printf("<< circular queue operations where the initial capacity is 2>>\na 1 Jung\nd\n\n");
	printf("******************************************************\n");
	while (1) {
		gets(input);
		sscanf(input, "%s %d %s", temp, &te.no, te.name);
		if (strcmp(temp, "a") == 0) {
			addq(te);
		}
		else if (strcmp(temp, "d") == 0) {
			deleteq();
			printf("delete elements : %d %s\n", queue[front].no, queue[front].name);
		}
	}
}