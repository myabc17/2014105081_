#include <stdio.h>
#include <stdlib.h>
#define MAX_QUEUE_SIZE 5
#define MAX_NAME_SIZE 81

typedef struct {
	int no;
	char name[MAX_NAME_SIZE];
}element;
element queue[MAX_QUEUE_SIZE];
int rear = -1;
int front = -1;

void print(element temp[])
{
	int i;
	printf("current stack elements : \n");
	for (i = 0; i < MAX_QUEUE_SIZE; i++) {
		printf("%d %s\n", queue[i].no, queue[i].name);
	}
	exit(EXIT_FAILURE);
}

void queueFull()
{
	int i, j = 0;
	if (front == -1) {
		printf("Queue is full, cannot add element!\n");
		print(queue);
	}
	else {
		for (i = front + 1; i <= rear; i++) {
			queue[j] = queue[i];
			j += 1;
		}
		rear = rear - (front + 1);
		front = -1;
	}
}

element queueEmpty()
{
	printf("Queue is empty, cannot delete elemnet.\n");
	exit(EXIT_FAILURE);
}

void addq(element item)
{
	if (rear == MAX_QUEUE_SIZE - 1)
		queueFull();
	queue[++rear] = item;
}

element deleteq()
{
	if (front == rear)
		return queueEmpty();
	return queue[++front];
}

void main()
{
	char input[81];
	char check[81];
	element temp;
	printf("<< linear queue operations where MAX_QUEUE_SIZE is 5>>\na 1 Jung\nd\n");
	printf("******************************************\n");
	while (1) {
		gets(input);
		sscanf(input, "%s %d %s", check, &temp.no, temp.name);
		if (strcmp(check,"a") == 0) {
			addq(temp);
		}
		else if (strcmp(check, "d") == 0) {
			deleteq();
			printf("delete elements : %d %s\n", queue[front].no, queue[front].name);
		}
	}
}