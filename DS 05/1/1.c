#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_SIZE 5
#define MAX_NAME_SIZE 81

typedef struct {
	int no;
	char name[MAX_NAME_SIZE];
}element;
element stack[MAX_STACK_SIZE];
int top = -1;

void print(element temp[])
{
	int i;
	printf("current stack elements : \n");
	for (i = MAX_STACK_SIZE; i >= 0; i--) {
		printf("%d %s\n", stack[i].no, stack[i].name);
	}
}

void stackFull()
{
	printf("Stack is full, cannot add element\n");
	print(stack);
}

void push(element item)
{
	if (top >= MAX_STACK_SIZE - 1)
		stackFull();
	stack[++top] = item;
}

element stackEmpty()
{
	printf("stack is empty, cannot delete elemnet.\n");
	exit(EXIT_FAILURE);
}

element pop()
{
	if (top == -1)
		return stackEmpty();
	return stack[top--];
}



void main()
{
	char input[81], temp[81];
	element temp2;
	int i;

	printf("<< stack operations where MAX_STACK_SIZE is 5>>\n");
	printf("push 1 Jung\npop\n\n**************************************\n");
	while (1) {
		gets(input);
		sscanf(input, "%s %d %s", temp, &temp2.no, temp2.name);
		if (strcmp(temp, "push") == 0) {
			push(temp2);
			if (top == MAX_STACK_SIZE)
				break;
		}
			
		else if (strcmp(temp, "pop") == 0) {
			printf("delete elemnet : %d %s\n", stack[top].no, stack[top].name);
			pop();
		}
	}
	
}
