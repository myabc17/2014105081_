#include <stdio.h>
#include <stdlib.h>
#define FALSE 0
#define TRUE 1
#define MAX_VERTICES 10

FILE *f;
short int visited[MAX_VERTICES];
typedef struct queue *queuePointer;
typedef struct queue {
	int vertex;
	queuePointer link;
}queue;
queuePointer front, rear;
queuePointer graph[MAX_VERTICES];

void addq(int item)
{
	queuePointer temp;
	temp = (queuePointer)malloc(sizeof(*temp));
	temp->vertex = item;
	temp->link = NULL;
	if (front)
		rear->link = temp;
	else
		front = temp;
	rear = temp;
}
void bfs(int v)
{
	queuePointer w;
	front = rear = NULL;
	printf("%3d", v);
	visited[v] = TRUE;
	addq(v);
	while (front) {
		v = deleteq();
		for (w = graph[v]; w;w=w->link)
		if (!visited[w->vertex]) {
			printf("%3d", w->vertex);
			addq(w->vertex);
			visited[w->vertex] = TRUE;
		}
	}
}
int queueEmpty()
{
	fprintf(stderr, "queue is empty.\n");
	exit(EXIT_FAILURE);
}
int deleteq()
{
	queuePointer temp = front;
	int item;
	if (!temp)
		return queueEmpty();
	item = temp->vertex;
	front = temp->link;
	free(temp);
	return item;
}
void printgraph(queuePointer head, int index)
{
	queuePointer temp;
	temp = head;
	printf("adjList[%d] : ", index);
	for (; temp; temp = temp->link) {
		printf("%3d", temp->vertex);
	}
	printf("\n");
}
void insert()
{
	int a, b;
	queuePointer temp, x;
	while (fscanf(f, "%d %d", &a, &b) != -1) {
		temp = (queuePointer)malloc(sizeof(*temp));
		temp->vertex = b;
		temp->link = NULL;
		if (!graph[a])
			graph[a] = temp;
		else {
			temp->link = graph[a];
			graph[a] = temp;
		}
		temp = (queuePointer)malloc(sizeof(*temp));
		temp->vertex = a;
		temp->link = NULL;
		if (!graph[b])
			graph[b] = temp;
		else {
			temp->link = graph[b];
			graph[b] = temp;
		}
	}
}

void main()
{
	int i, j, vertex, edge;
	f = fopen("input.txt", "r");
	fscanf(f, "%d %d", &vertex, &edge);
	insert();
	printf("<<< Adjacency List >>>\n");
	for (i = 0; i < vertex; i++) {
		printgraph(graph[i], i);
	}
	printf("<<< Depth First Search >>>\n");
	for (i = 0; i < vertex; i++) {
		printf("dfs<%d> : ", i);
		bfs(i);
		printf("\n");
		for (j = 0; j < vertex; j++)
			visited[j] = 0;
	}
	fclose(f);
}