#include <stdio.h>
#define FALSE 0
#define TRUE 1
#define MAX_VERTICES 10

short int visited[MAX_VERTICES];
FILE *f;
typedef struct node *nodePointer;
typedef struct node {
	short int vertex;
	nodePointer link;
}node;
nodePointer graph[MAX_VERTICES];

void dfs(int v)
{
	nodePointer w;
	visited[v] = TRUE;
	printf("%3d", v);
	for (w = graph[v]; w; w=w->link)
	if (!visited[w->vertex])
		dfs(w->vertex);
}

nodePointer findLast(nodePointer head)
{
	nodePointer temp;
	temp = head;
	for (; temp->link; temp = temp->link);
	return temp;
}
void insert()
{
	int a, b;
	nodePointer temp, x;
	while (fscanf(f, "%d %d", &a ,&b) != -1) {
		temp = (nodePointer)malloc(sizeof(*temp));
		temp->vertex = b;
		temp->link = NULL;
		if (!graph[a])
			graph[a] = temp;
		else {
			temp->link = graph[a];
			graph[a] = temp;
		}
		temp = (nodePointer)malloc(sizeof(*temp));
		temp->vertex = a;
		temp->link = NULL;
		if (!graph[b])
			graph[b] = temp;
		else {
			temp->link = graph[b];
			graph[b] = temp;
		}
	}
}
void printgraph(nodePointer head, int index)
{
	nodePointer temp;
	temp = head;
	printf("adjList[%d] : ", index);
	for (; temp; temp = temp->link) {
		printf("%3d", temp->vertex);
	}
	printf("\n");
}
void main()
{
	int i, j, vertex, edge;
	f = fopen("input.txt", "r");
	fscanf(f, "%d %d", &vertex, &edge);
	insert();
	printf("<<< Adjacency List >>>\n");
	for (i = 0; i < vertex; i++) {
		printgraph(graph[i], i);
	}
	printf("<<< Depth First Search >>>\n");
	for (i = 0; i < vertex; i++) {
		printf("dfs<%d> : ", i);
		dfs(i);
		printf("\n");
		for (j = 0; j < vertex; j++)
			visited[j] = 0;
	}
	fclose(f);
}