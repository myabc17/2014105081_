#include <stdio.h>
#include <string.h>

int nfind(char *string, char *pat)
{
	int i, j = 0, start = 0;
	int lasts = strlen(string) - 1;
	int lastp = strlen(pat) - 1;
	int endmatch = lastp;

	for (i = 0; endmatch <= lasts; endmatch++, start++) {
		if (string[endmatch] == pat[lastp]) {
			for (j = 0, i = start; j < lastp && string[i] == pat[j]; i++, j++);
		}
		if (j == lastp)
			return start;
	}
	return -1;
}

void main()
{
	FILE *fin = fopen("string.txt", "r"), *fout = fopen("pattern.txt", "r");
	char a[81], b[81];
	int i, temp = 0, count = 0, temp2 = 0;

	fgets(a, 81, fin);
	fgets(b, 81, fout);
	printf("string : %s\n", a);
	printf("pattern : %s\n\n", b);
	for (i = 0;i<10; i++) {
		if (nfind(a + temp, b) != -1) {
			if (count == 0)
				temp2 += nfind(a + temp, b);
			else
				temp2 += nfind(a + temp, b) + strlen(b);
			temp += nfind(a + temp, b) + strlen(b);
			printf("matching position %d : %d\n", i + 1, temp2);
			count++;
		}
		else
			break;
	}
	printf("\nthe number of pattern matching : %d\n", count);
}