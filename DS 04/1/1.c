#include <stdio.h>
#define MAX_COL 10
#define MAX_TERMS 101
typedef struct
{
	int row;
	int col;
	int value;
}term;
term a[MAX_TERMS];
void fastTranspose(term a[], term b[])
{
	int rowTerms[MAX_COL], startingPos[MAX_COL];
	int i, j, numCols = a[0].col; b[0].col = a[0].row;
	int numTerms = a[0].value;
	b[0].row = numCols;
	b[0].value = numTerms;
	if (numTerms > 0)
	{
		for (i = 0; i < numCols; i++)
			rowTerms[i] = 0;
		for (i = 1; i <= numTerms; i++)
			rowTerms[a[i].col]++;
		startingPos[0] = 1;
		for (i = 1; i < numCols; i++)
			startingPos[i] = startingPos[i - 1] + rowTerms[i - 1];
		for (i = 1; i <= numTerms; i++) {
			j = startingPos[a[i].col]++;
			b[j].row = a[i].col;
			b[j].col = a[i].row;
			b[j].value = a[i].value;
		}
	}
}

int** Create(maxRow, maxCol) 
{
	int **x, i;
	x = (int**) calloc(maxRow, sizeof(*x));
	for (i = 0; i < maxRow; i++) {
		x[i] = (int*)calloc(maxCol, sizeof(**x));
	}
	return x;
}

void print(term a[], int row, int col)
{
	int i, j, temp=0, temp2=0;
	for (i = 0; i < row; i++) {
		int count = 0;
		for (j = 0; j < col; j++) {
			if ((a[temp + 1].col == j) && (a[temp2 + 1].row == a[temp + 1].row) && a[temp2 + 1].row == i) {
				printf("%d\t", a[temp + 1].value);
				temp += 1;
				count += 1;
			}
			else
				printf("%d\t", 0);
		}
		temp2 += count;
		printf("\n");
	}
}

void main()
{
	FILE *fin = fopen("a.txt", "r"), *fout = fopen("b.txt", "w");
	int a, i, j, p, q;
	term input[MAX_TERMS],output[MAX_TERMS];

	fscanf(fin, "%d %d %d", &input[0].row, &input[0].col, &input[0].value);
	a = Create(input[0].row, input[0].col);
	for (i = 1; i <= input[0].value; i++) {
		fscanf(fin,"%d %d %d", &input[i].row, &input[i].col, &input[i].value);
	}
	fastTranspose(input, output);
	printf("A\n");
	print(input, input[0].row, input[0].col);
	printf("\nB\n");
	print(output, output[0].row, output[0].col);
}