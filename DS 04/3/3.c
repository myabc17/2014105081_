#include <stdio.h>
#include <string.h>
#define max_string_size 100
#define max_pattern_size 100
int failure[max_pattern_size];
char string[max_string_size];
char pat[max_pattern_size];

void fail(char *pat)
{
	int i = 0, j = 0;
	int n = strlen(pat);
	failure[0] = -1;

	for (j = 1; j < n; j++) {
		i = failure[j - 1];
		while ((pat[j] != pat[i + 1]) && (i >= 0)) {
			i = failure[i];
		}
		if (pat[j] == pat[i + 1]) {
			failure[j] = i + 1;
		}
		else {
			failure[j] = -1;
		}
	}
}

void print(char *pat)
{
	int i = 0, j = 0;
	int n = strlen(pat);
	failure[0] = -1;
	fail(pat);
	printf("j\t");
	for (j = 0; j < strlen(pat); j++)
		printf("%d\t", j);
	printf("\npat\t");
	for (j = 0; j < strlen(pat); j++)
		printf("%c\t", pat[j]);
	printf("\nf\t");
	for (j = 0; j < strlen(pat); j++)
		printf("%d\t", failure[j]);
	printf("\n\n");
}
int pmatch(char *string, char *pat)
{
	int i = 0, j = 0;
	int lens = strlen(string);
	int lenp = strlen(pat);
	while (i < lens && j < lenp) {
		if (string[i] == pat[j]) {
			i++;
			j++;
		}
		else if (j == 0) {
			i++;
		}
		else {
			j = failure[j - 1] + 1;
		}
	}
	return ((j == lenp) ? (i - lenp) : -1);
}

void main()
{
	FILE *fin = fopen("string.txt", "r"), *fout = fopen("pattern.txt", "r");
	char a[81], b[81];
	int i, temp = 0, count = 0, temp2 = 0;

	fgets(a, 81, fin);
	fgets(b, 81, fout);
	printf("string : %s\n", a);
	printf("pattern : %s\n\n", b);
	fail(b);
	print(b);
	for (i = 0; i<10; i++) {
		if (pmatch(a + temp, b) != -1) {
			if (count == 0)
				temp2 += pmatch(a + temp, b);
			else
				temp2 += pmatch(a + temp, b) + strlen(b);
			temp += pmatch(a + temp, b) + strlen(b);
			printf("matching position %d : %d\n", i + 1, temp2);
			count++;
		}
		else
			break;
	}
	printf("\nthe number of pattern matching : %d\n", count);
}