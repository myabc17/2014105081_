#include <stdio.h>
#include <stdlib.h>
#define MAX_ELEMENTS 200

typedef struct {
	int key;
}element;
element heap[MAX_ELEMENTS];
element heap2[MAX_ELEMENTS];
int winner[MAX_ELEMENTS];

void main()
{
	int seed, k, s, i, j, q, temp, count = 0, min, sizeofK;
	int **run;
	printf("입력하시오. ex ) (seed, k(run의 개수), s(run의 크기) : ");
	scanf("%d %d %d",&seed, &k, &s);
	srand(seed);
	sizeofK = k;
	run = (int**)malloc(k*sizeof(*run));	//2차원배열 동적할당
	for(i=0; i<k; i++) {
			run[i] = (int*)malloc(s*sizeof(**run));
		for(j=0; j<s; j++) {
			run[i][j] = rand() % 100 + 1;
			//printf("%3d",run[i][j]);
		}
		
		//printf("\n");
	}
	printf("sorting...\n");
	for(i=0; i<k; i++) {					//각 run들 정렬
		for(j=0; j<s; j++) {
			for(q=j; q < s; q++) {
				if(run[i][j] >= run[i][q]) {
					temp = run[i][j];
					run[i][j] = run[i][q];
					run[i][q] = temp;
				}
			}
		}
		heap[i].key = run[i][0];
	}
	for(i=0; i<k; i++) {					//run들의 값과 최솟값(heap) 출력
		for(j=0; j<s; j++) {
			printf("%3d",run[i][j]);
		}
		printf("      min : %3d\n",heap[i].key);
	}


	while(count != 24) {
		for(i=0;i<k;i++) {					
			if(run[i][0] == NULL)
				heap[i].key = 101;
			else
				heap[i].key = run[i][0];	//heap에 각 run의 승자 저장
			heap2[i+8].key = heap[i].key;	//heap2의 승자트리를 만들기위해 저장
		}
		
		min = heap[0].key;					//heap의 초기 최솟값 min
		
		for(i=0;i<k;i++) {					//heap의 최솟값 find & 그 값의 index를 j에 저장
			if(min >= heap[i].key) {
				min = heap[i].key;
				j = i;
			}
		}
		
		winner[count] = min;				//승자트리의 값 정수배열 저장
		
		for(i=0;i<s;i++) {					//각 run들을 앞으로 한칸씩 당김
			run[j][i] = run[j][i+1];
			run[j][i+1] = 101;
			if (run[j][i + 2] > 100 || run[j][i+2] < 1)
				break;
		}
		
		sizeofK = k;
		while(sizeofK != 1) {				//승자트리 구성
			for(i=sizeofK; i<sizeofK*2; i += 2) {
				if(heap2[i].key < heap2[i+1].key)
					heap2[i/2].key = heap2[i].key;
				else
					heap2[i/2].key = heap2[i+1].key;
			}
			sizeofK /= 2;
		}
		printf("\n %d 단계의 승자트리\n",count+1);
		for(i=1;i<k*2;i++)
			printf("%4d",heap2[i]);
		printf("\n");
		count++;	
	}
	
	printf("\n각 run단계별 winner 출력\n");
	
	for (i = 0; i<count; i++)
		printf("%3d", winner[i]);
	printf("\n");
}