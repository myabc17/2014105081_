#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX_ELEMENTS 200
#define MAX_STACKS 100
#define HEAP_FULL(n) (n == MAX_ELEMENTS-1)
#define HEAP_EMPTY(n) (!n)

typedef struct {
	int key;
}element;
typedef struct node *treePointer;
typedef struct node{
	element data;
	treePointer leftChild, rightChild;
}node;
treePointer root = NULL;
element heap[MAX_ELEMENTS];
treePointer stack[MAX_STACKS];
int n = 0;
int top = -1;

void main()
{
	int seed, k, i, j, size;
	int temp;
	printf("input seed, k개의 난수 : ");
	scanf("%d %d",&seed, &k);
	srand(seed);
	printf("인덱스 값 : ");
	for(i = k; i < k*2; i++)
		printf("%3d",i);
	printf("\n");
	printf(" 난수  값 : ");
	for(i=k; i<k*2; i++) {
		heap[i].key = rand() % 100 + 1;
		printf("%3d",heap[i].key);
	}
	size = k*2;
	while(k != 1) {
		for(i=k; i<k*2; i += 2) {
			if(heap[i].key < heap[i+1].key)
				heap[i/2].key = heap[i].key;
			else
				heap[i/2].key = heap[i+1].key;
		}
		k /= 2;
	}
	printf("\n\nThe initial winner tree includes followings : \n");
	printf("인덱스 값 : ");
	for(i = 1; i < size; i++)
		printf("%3d",i);
	printf("\n");
	printf(" 트리  값 : ");
	for(i = 1; i < size; i++)
		printf("%3d",heap[i].key);
	printf("\n");
}
