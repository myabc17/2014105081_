#include <stdio.h>

void add(int **a, int **b, int **c, int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			c[i][j] = a[i][j] + b[i][j];
		}
	}
}
int** make2dArray(int rows, int cols)
{
	int **x, i;
	x = malloc(sizeof(*x)*rows);
	for (i = 0; i < rows; i++)
		x[i] = malloc(cols*sizeof(**x));
	return x;
}
void init2dArray(int **ary, int rows, int cols)
{
	int i, j;
	srand(time(0));
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
			ary[i][j] = rand() % 10;
	}
}
void print2dArray(int **ary, int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
			printf("%d ", ary[i][j]);
		printf("\n");
	}
	printf("\n");
}
void free2dArray(int **ary, int rows)
{
	int i;
	for (i = 0; i < rows; i++)
	{
		free(ary[i]);
		ary[i] = NULL;
	}
	free(ary);
}
void main()
{
	int **a, **b, **c, i, j;
	int rows, cols;
	printf("Enter row size and column size (ex) 3 4 : ");
	scanf("%d%d", &rows, &cols);
	a = make2dArray(rows, cols);
	init2dArray(a, rows, cols);
	printf("matrix A\n");
	print2dArray(a, rows, cols);
	b = make2dArray(rows, cols);
	init2dArray(b, rows, cols);
	printf("matrix B\n");
	print2dArray(b, rows, cols);
	c = make2dArray(rows, cols);
	add(a, b, c, rows, cols);
	printf("matrix C\n");
	print2dArray(c, rows, cols);
	free2dArray(a, rows);
	free2dArray(b, rows);
	free2dArray(c, rows);
	printf("2d array - free!!!\n");
}