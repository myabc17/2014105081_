#include <stdio.h>

void init2dArray(int ary[][4], int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
			ary[i][j] = i + j;
	}
}
void print2dArray(int *ary, int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
			printf("%d ", *(ary+i)+j);
		printf("\n");
	}
}
void main()
{
	int ary[3][4];

	init2dArray(ary, 3, 4);
	print2dArray(ary, 3, 4);
}