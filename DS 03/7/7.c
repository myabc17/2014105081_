#include <stdio.h>
#include <stdlib.h>
#define MAX_TERMS 100
typedef struct
{
	float coef;
	int expon;
}term;
term terms[MAX_TERMS];
int avail = 0;

int COMPARE(int ex1, int ex2)
{
	if (ex1 < ex2)
	{
		return -1;
	}
	else if (ex1 == ex2)
	{
		return 0;
	}
	else if (ex1 > ex2)
	{
		return 1;
	}
}

void attach(float coeff, int expo)
{
	if (avail >= MAX_TERMS)
	{
		fprintf(stderr, "Too many terms in the polynomials\n");
		exit(EXIT_FAILURE);
	}
	terms[avail].coef = coeff;
	terms[avail++].expon = expo;
}


void padd(int sA, int fA, int sB, int fB, int *sD, int *fD)
{
	float coefficient;
	*sD = avail;
	while (sA <= fA&&sB <= fB)
	{
		switch (COMPARE(terms[sA].expon, terms[sB].expon))
		{
		case -1:
			attach(terms[sB].coef, terms[sB].expon);
			sB++;
			break;
		case 0:
			coefficient = terms[sA].coef + terms[sB].coef;
			sA++;
			sB++;
			break;
		case 1:
			attach(terms[sA].coef, terms[sA].expon);
			sA++;
		}
	}
	for (; sA <= fA; sA++)
	{
		attach(terms[sA].coef, terms[sA].expon);
	}
	for (; sB <= fB; sB++)
	{
		attach(terms[sB].coef, terms[sB].expon);
	}
	*fD = avail - 1;
}

void main()
{
	FILE *f = fopen("input.txt", "r");
	int i, j, k;
	int startD, finishD;
	int a, b;
	fscanf(f, "%d %d", &a, &b);
	printf("\nInput in descending order\n");
	for (i = 0; i < a; i++)
	{
		fscanf(f, "%f %d", &terms[i].coef, &terms[i].expon);
	}
	printf("\n");
	for (j = a; j < a + b; j++)
	{
		fscanf(f, "%f %d", &terms[j].coef, &terms[j].expon);
	}
	printf("<< D(x) = A(x) + B(x) >>\n");
	printf("A(x) = ");
	for (i = 0; i < a; i++)
	{
		if (terms[i].expon == 0)
		{
			printf("%f", terms[i].coef);
			break;
		}
		printf("%fx^%d", terms[i].coef, terms[i].expon);
		printf(" + ");
	}
	printf("\nB(x) = ");
	for (j = a; j < a + b; j++)
	{
		if (terms[j].expon == 0)
		{
			printf("%f", terms[j].coef);
			break;
		}
		printf("%fx^%d", terms[j].coef, terms[j].expon);
		printf(" + ");
	}
	printf("\nD(x) = ");
	padd(0, a - 1, a - 1, a + b, &startD, &finishD);
	for (k = startD; k < finishD; k++)
	{
		if (terms[k].expon == 0)
		{
			printf("%f\n", terms[k].coef);
			break;
		}
		printf("%fx^%d", terms[k].coef, terms[k].expon);
		printf(" + ");
	}
}