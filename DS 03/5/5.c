#include <stdio.h>

#define FALSE 0
#define TRUE 1

typedef struct
{
	char name[81];
	int age;
	int salary;
}humanBeing;
int humanEqual(humanBeing person1, humanBeing person2)
{
	if (strcmp(person1.name, person2.name))
		return FALSE;
	if (person1.age != person2.age)
		return FALSE;
	if (person1.salary != person2.salary)
		return FALSE;
	return TRUE;
}
void main()
{
	int i;
	humanBeing a[2];
	for (i = 0; i < 2; i++)
	{
		printf("Input person%d's name, age, salary :\n",i+1);
		gets(a[i].name);
		fflush(stdin);
		scanf("%d", &a[i].age);
		fflush(stdin);
		scanf("%d", &a[i].salary);
		fflush(stdin);
	}
	if (humanEqual(a[0], a[1]))
		printf("The two human beings are the same\n");
	else
		printf("The two human beings are not the same\n");
}