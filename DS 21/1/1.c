#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 11
 
typedef struct element {
	char item[20];
	int key;
}element;

element *ht[MAX_SIZE] = { NULL };
int numberofcomp = 1;
FILE *f;
int b = 11, s = 1;
unsigned int stringToInt(char *key)
{
	int number = 0;
	while (*key)
		number += *key++;
	return number;
}
int h(int k)
{
	return k%b;
}
element* search(int k)
{
	int homeBucket, currentBucket;
	homeBucket = h(k);
	for (currentBucket = homeBucket; ht[currentBucket] && ht[currentBucket]->key != k;) {
		numberofcomp++;
		currentBucket = (currentBucket + 1) % b;
		if (currentBucket == homeBucket)
			return NULL;
	}
	if (ht[currentBucket]->key == k)
		return ht[currentBucket];
	return NULL;
}
void insert(element *d, int size)
{
	int i, input, k;
	element *temp, *check;
	input = h(d->key);
	
	if(!ht[input])
		ht[input] = d;
	else {
		for (k = input;; k++) {
			if (ht[k] == NULL) {
				ht[k] = d;
				break;
			}
		}
	}
}


void main()
{
	f = fopen("input.txt", "r");
	char temp[20];
	int i, count = 0, check = 0;
	element *in, *out, *input;
	printf("input strings : ");
	while (fscanf(f, "%s", temp) != EOF) {
		printf("%s ", temp);
		in = (element *)malloc(sizeof(element));
		in->key = stringToInt(temp);
		strcpy(in->item, temp);
		insert(in, count);
		count++;
	}
	printf("\n\n%20s%7s\n", "item","key");
	for (i = 0; i <= count; i++) {
		if(ht[i])
			printf("ht[%2d] : %11s%7d\n", i, ht[i]->item, ht[i]->key);
		else
			printf("ht[%2d] : %11s%7s\n", i, "" , "");
	}
	
	printf("\nstarting to search >> ");
	scanf("%s", temp);
	for (i = 0; i <= count; i++) {
		if(ht[i])
			if (strcmp(ht[i]->item,temp) == 0) {
				input = search(ht[i]->key);
				printf("item: %s, key: %d, the number of comparitions : %d\n",ht[i]->item,ht[i]->key, numberofcomp);
				check = 1;
				break;
		}
	}
	if (check == 0)
		printf("it doesn't exist!\n");
}