#include <stdio.h>
#include <stdlib.h>
#define MAX_ELEMENTS 200
#define HEAP_FULL(n) (n == MAX_ELEMENTS-1)
#define HEAP_EMPTY(n) (!n)

typedef struct {
	int key;
}element;
element heap[MAX_ELEMENTS];
int n = 0;


void push(element item, int *n)
{
	int i;
	if (HEAP_FULL(*n)) {
		fprintf(stderr, "The heap is full. \n");
		exit(EXIT_FAILURE);
	}
	i = ++(*n);
	while ((i != 1) && (item.key > heap[i / 2].key)) {
		heap[i] = heap[i / 2];
		i /= 2;
	}
	heap[i] = item;
}
element pop(int *n)
{
	int parent, child;
	element item, temp;
	if (HEAP_EMPTY(*n)) {
		fprintf(stderr, "The heap is empty\n");
		exit(EXIT_FAILURE);
	}
	item = heap[1];
	temp = heap[(*n)--];
	parent = 1;
	child = 2;
	while (child <= *n) {
		if ((child < *n) && (heap[child].key < heap[child + 1].key))
			child++;
		if (temp.key >= heap[child].key) break;
		heap[parent] = heap[child];
		parent = child;
		child *= 2;
	}
	heap[parent] = temp;
	return item;
}

void main()
{
	FILE *f = fopen("input.txt", "r");
	element temp2;
	int i = 0, j, temp, d, k, size;
	printf("***** insertion into a max heap *****\n");
	while (temp = fscanf(f, "%3d", &d) != -1) {
		temp2.key = d;
		push(temp2, &n);
		i++;
		for (j = 0; j < i; j++) {
			printf("%d ", heap[j+1].key);
		}
		printf("\n");
	}
	size = i;
	printf("***** deletion from a max heap *****\n");
	for (j = 0; j < size; j++) {
		heap[i] = pop(&n);
		i--;
		for (k = 0; k < i; k++) {
			printf("%3d", heap[k + 1].key);
		}
		printf("\n");
	}
	fclose(f);
}