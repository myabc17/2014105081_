#include <stdio.h>
#define MAX_STACKS 100
typedef int iType;
typedef struct {
	int key;
	iType item;
}element;

typedef struct node *treePointer;
typedef struct node{
	element data;
	treePointer leftChild, rightChild;
}node;
treePointer root = NULL;
treePointer stack[MAX_STACKS];
int top = -1;
treePointer pop(void)
{
	return stack[top--];
}
void push(treePointer item)
{
	stack[++top] = item;
}
treePointer eval(int a)
{
	treePointer temp, temp2;
	temp = (treePointer)calloc(1, sizeof(*temp));
	char symbol;
	int n = 0;
	top = -1;
	temp->data.key = a;
	temp->leftChild = temp->rightChild = NULL;
	push(temp);
	return stack[0];
}
void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%d ", ptr->data.key);
		inorder(ptr->rightChild);
	}
}
element* search(treePointer root, int k)
{
	if (!root) return NULL;
	if (k == root->data.key) return &(root->data);
	if (k < root->data.key)
		return search(root->leftChild, k);
	return search(root->rightChild, k);
}
element* modifiedSearch(treePointer tree, int k)
{
	while (tree) {
		if (k == tree->data.key) return &(tree->data);
		
		if (k < tree->data.key) {
			if (tree->leftChild == NULL)
				return tree;
			else
				tree = tree->leftChild;
		}
		else
			if (tree->rightChild == NULL)
				return tree;
			else
				tree = tree->rightChild;
	}
	return NULL;
}

void insert(treePointer *node, int k, iType theItem)
{
	treePointer ptr, temp = modifiedSearch(*node, k);
	if (temp || !(*node)) {
		ptr = (treePointer)malloc(sizeof(*ptr));
		ptr->data.key = k;
		ptr->data.item = theItem;
		ptr->leftChild = ptr->rightChild = NULL;
		if (*node)
		if (k < temp->data.key) temp->leftChild = ptr;
		else temp->rightChild = ptr;
		else *node = ptr;
	}
}

void main()
{
	
	//root = (treePointer)malloc(sizeof(*root));
	//root->leftChild = root->rightChild = NULL;
	//root->data.item = root->data.key = 0;
	int i, j, temp, input, n, seed, ch;
	int *store;
	printf("random number generation (1~500)\n");
	printf("the number of nodes in BST (less than 50) : ");
	scanf("%d", &n);
	store = (int)malloc(n * sizeof(int));
	printf("seed : ");
	scanf("%d", &seed);
	srand(seed);
	for (i = 0; i < n; i++) {
		input = rand() % 500 + 1;
		store[i] = input;
	}

	for (i = 0; i < n; i++) {
		//printf("%4d", store[i]);
		insert(&root, store[i], store[i]);
	}
	printf("\ncreating a BST from random numbers\nthe key to search : ");
	scanf("%d", &ch);
	if (search(root, ch) == NULL)
		printf("there is no such element\n\n");
	else
		printf("there is such element\n\n");
	printf("inorder traversal of the BST shows the sorted sequence\n");
	inorder(root);
	printf("\n");
}