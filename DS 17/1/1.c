#include <stdio.h>
#include <stdlib.h>
#define MAX_VERTICES 10

typedef struct node *nodePointer;
typedef struct node {
	int vertex;
	nodePointer link;
};
typedef struct {
	int count;
	nodePointer link;
}hdnodes;
hdnodes graph[MAX_VERTICES];

FILE *f;

void topSort(hdnodes graph[], int n)
{
	int i, j, k, top;
	nodePointer ptr;
	top = -1;
	for (i = 0; i < n;i++)
	if (!graph[i].count) {
		graph[i].count = top;
		top = i;
	}
	for (i = 0; i < n; i++)
	if (top == -1) {
		fprintf(stderr, "\nNetwork has a cycle. Sort terminated. \n");
		exit(EXIT_FAILURE);
	}
	else {
		j = top;
		top = graph[top].count;
		printf("v%d,  ", j);
		for (ptr = graph[j].link; ptr; ptr = ptr->link) {
			k = ptr->vertex;
			graph[k].count--;
			if (!graph[k].count) {
				graph[k].count = top;
				top = k;
			}
		}
	}
}
nodePointer findLast(hdnodes graph)
{
	nodePointer ptr;
	ptr = graph.link;
	for (; ptr->link; ptr = ptr->link);
	return ptr;
}
void insert()
{
	nodePointer temp, x;
	int vertex, n;
	while (fscanf(f, "%d %d", &n, &vertex) != -1) {
		temp = (nodePointer)malloc(sizeof(*temp));
		temp->link = NULL;
		temp->vertex = vertex;
		switch (vertex) {
		case 1:
			graph[vertex].count++;
			break;
		case 2:
			graph[vertex].count++;
			break;
		case 3:
			graph[vertex].count++;
			break;
		case 4:
			graph[vertex].count++;
			break;
		case 5:
			graph[vertex].count++;
			break;
		}
		if (graph[n].link == NULL)
			graph[n].link = temp;
		else {
			temp->link = graph[n].link;
			graph[n].link = temp;
		}

	}
}
void main()
{
	f = fopen("input.txt", "r");
	int i, vertexsize, edgesize;
	fscanf(f, "%d %d", &vertexsize, &edgesize);
	for (i = 0; i < vertexsize; i++) {
		graph[i].link = NULL;
	}
	
	insert();
	topSort(graph, vertexsize);
	printf("\nok\n\n");
}