#include <stdio.h>
#include <stdlib.h>
#define MAX_STACKS 3

typedef struct
{
	int id;
	int grade;
} element;
typedef struct stack *stackPointer;
typedef struct stack
{
	element data;
	stackPointer link;
} Node;

stackPointer top[MAX_STACKS];

element stackEmpty();
void push(int i, element item);
element pop(int i);

void main()
{
	FILE *f = fopen("input.txt", "r");
	element item;
	int i, id, grade;

	while (!feof(f)) {
		fscanf(f, "%d %d %d", &i, &item.id, &item.grade);
		push(i, item);
	}

	printf("과목번호, 학번, 성적\n");

	for (i = 0; i < MAX_STACKS; i++) {
		printf("**********************\n");
		while (1) {
			item = pop(i);
			if (item.id == -1)
				break;
			printf("%8d %5d %5d\n", i, item.id, item.grade);
		}
	}
	fclose(f);
}

element stackEmpty()
{
	element temp;
	temp.id = -1;
	return temp;
}

void push(int i, element item)
{
	stackPointer temp;
	temp = (stackPointer)malloc(sizeof(*temp));
	temp->data = item;
	temp->link = top[i];
	top[i] = temp;
}

element pop(int i)
{
	stackPointer temp = top[i];
	element item;
	
	if (!temp)
		return stackEmpty();
	item = temp->data;
	top[i] = temp->link;
	free(temp);
	return item;
}