#include <stdio.h>
#include <stdlib.h>

typedef struct listNode {
	int data;
	struct listNode *link;
} listNode;

listNode *first = NULL;

void insert(listNode **first, listNode *x, int num)
{
	listNode *temp;
	temp = (listNode *)malloc(sizeof(*temp));
	temp->data = num;
	if (*first) {
		if (x == NULL) {
			temp->link = *first;
			*first = temp;
		}
		else {
			temp->link = x->link;
			x->link = temp;
		}
	}
	else {
		temp->link = NULL;
		*first = temp;
	}
}

listNode *find(listNode **first, listNode *a, int num)
{
	listNode *search;
	a = (listNode *)malloc(sizeof(*a));
	a->link = NULL;
	search = *first;

	if (*first == NULL) {
		return search;
	}
	if (num < ((*first)->data)) {
		a->data = num;
		return NULL;
	}
	else {
		while (1) {
			if (num == search->data) {
				a = search;
				a->data = num;
				return search;
			}
			else if (search->link == NULL) {
				a->data = num;
				return search;
			}
			else if (num < (search->link->data)) {
				a->data = num;
				return search;
			}
			search = search->link;
		}
	}
}

void delete1(listNode **first, listNode *trail, listNode *x)
{
	if (trail) {
		trail->link = x->link;
	}
	else {
		*first = (*first)->link;
	}
	free(x);
}

void printList(listNode *first)
{
	int i = 0;

	for (i = 1; first; first = first->link, i++) {
		printf("%4d", first->data);
		if (i % 10 == 0) {
			printf("\n");
		}
	}
	printf("\n");
}

void main()
{
	int num;
	listNode *a = NULL;
	listNode *b = NULL;
	FILE *f = fopen("input.txt", "r");
	while (fscanf(f, "%d", &num) != EOF)
	{
		insert(&first, find(&first, a, num), num);
	}
	printf("The ordered list contains:\n");
	printList(first);
	printf("After deleting nodes with data less than and equal to 50\nThe ordered list contains:\n");

	a = (listNode *)malloc(sizeof(*a));
	a = first;
	b = NULL;
	while (1) {
		if (a->data > 50) {
			break;
		}

		delete1(&first, b, a);
		a = first;
	}
	printList(first);

	fclose(f);
}