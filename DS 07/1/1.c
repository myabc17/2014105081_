#include <stdio.h>
#include <stdlib.h>
#define IS_EMPTY(first) (!(first))

typedef struct listNode *listPointer;
typedef struct listNode {
	int data;
	listPointer link;
} listNode;

listPointer find();
void create();
void insert(listPointer *first, listPointer x);
void delete(listPointer *first, listPointer trail, listPointer x);
void deleteNode_50();
void deleteNode_All();
void printList(listPointer first);

FILE *f;
listPointer first = NULL;


void main()
{
	listPointer x, temp;

	f = fopen("input.txt", "r");
	x = (listPointer)malloc(sizeof(*x));

	create();

	while (!feof(f))
		insert(&first, x);

	printf("The ordered list contain : \n");
	printList(first);

	printf("\nAfter deleting nodes with data less than and equal to 50\n");
	printf("The ordered list contain : \n");
	deleteNode_50();
	printList(first);

	deleteNode_All();
	fclose(f);

}

listPointer find(int num)
{
	listPointer temp = first;

	for (; temp->link != NULL; temp = temp->link)
		if (temp->link->data >= num)
			break;

	return temp;
}

void create()
{
	listPointer a, b;
	int temp;

	a = (listPointer)malloc(sizeof(*a));
	b = (listPointer)malloc(sizeof(*b));

	fscanf(f, "%d %d", &a->data, &b->data);

	if (a->data > b->data)
	{
		b->link = a;
		a->link = NULL;
		first = b;
	}
	else
	{
		a->link = b;
		b->link = NULL;
		first = a;
	}
}

void insert(listPointer *first, listPointer x)
{
	listPointer temp, linktemp;
	int num;

	temp = (listPointer)malloc(sizeof(*temp));
	fscanf(f, "%d", &num);

	x = find(num);

	temp->data = num;

	if (x == *first)
	{
		temp->link = *first;
		*first = temp;
	}
	else if (x->link == NULL)
	{
		x->link = temp;
		temp->link = NULL;
	}
	else
	{
		temp->data = num;
		temp->link = x->link;
		x->link = temp;
	}
}

void delete(listPointer *first, listPointer trail, listPointer x)
{
	if (trail)
		trail->link = x->link;
	else
		*first = (*first)->link;

	free(x);
}

void deleteNode_50()
{
	listPointer temp = first;

	for (; first->data <= 50; temp = first)
		delete(&first, NULL, temp);
}

void deleteNode_All()
{
	listPointer temp = first;

	for (; first; temp = first)
		delete(&first, NULL, temp);
}

void printList(listPointer first)
{
	int i;

	for (i = 1; first; first = first->link)
		printf("%4d%s", first->data, (i++ % 10) == 0 ? "\n" : (!(first->link) ? "\n" : ""));
}