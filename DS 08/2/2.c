#include <stdio.h>
#include <stdlib.h>
#define FALSE 0
#define TRUE 1

typedef struct polyNode *polyPointer;
typedef struct polyNode {
	int coef;
	int expon;
	polyPointer link;
}polyNode;
polyPointer a, b;
polyPointer c, lastA, lastB, avail = NULL;

void erase(polyPointer *ptr);
polyPointer getNode(void);
void retNode(polyPointer node);
void cerase(polyPointer *ptr);
polyPointer cpadd(polyPointer a, polyPointer b);
void insertFront(polyPointer *last, polyPointer node);
void attach(int coefficient, int exponent, polyPointer *ptr);
int COMPARE(int a, int b);
void printCList(polyPointer last);
void main()
{
	FILE *f1 = fopen("a.txt", "r");
	FILE *f2 = fopen("b.txt", "r");
	polyPointer c = NULL;
	polyNode temp;
	int check;
	a = NULL;
	b = NULL;
	while (1) {
		check = fscanf(f1, "%d %d", &temp.coef, &temp.expon);
		if (check == -1)
			break;
		insertFront(&a, c, temp.coef, temp.expon);
	}
	while (1) {
		check = fscanf(f1, "%d %d", &temp.coef, &temp.expon);
		if (check == -1)
			break;
		insertFront(&b, c, temp.coef, temp.expon);
	}
	c = cpadd(a, b);
	printf("\ta : ");
	printCList(&a);
	printf("\n\tb : ");
	printCList(&b);
	printf("\n\ta+b=c : ");
	printCList(&c);
	printf("\n");
	cerase(&a);
	cerase(&b);
	fclose(f1);
	fclose(f2);
}

void printCList(polyPointer last)
{
	polyPointer temp;
	if (last) {
		temp = last;
		do {
			if (a->coef > 0)
				printf("+%dx^%d\t", a->coef, a->expon);
			else if (a->coef < 0)
				printf("%dx^%d\t", a->coef, a->expon);
		} while (temp != last);
	}
}
void attach(int coefficient, int exponent, polyPointer *ptr)
{
	polyPointer temp;
	temp = getNode();
	temp->coef = coefficient;
	temp->expon = exponent;
	(*ptr)->link = temp;
	*ptr = temp;
}
void insertFront(polyPointer *last, polyPointer node)
{
	if (!(*last)) {
		*last = node;
		node->link = node;
	}
	else {
		node->link = (*last)->link;
		(*last)->link = node;
	}
}
void erase(polyPointer *ptr)
{
	polyPointer temp;
	while (*ptr) {
		temp = *ptr;
		*ptr = (*ptr)->link;
		free(temp);
	}
}
polyPointer getNode(void)
{
	polyPointer node;
	if (avail) {
		node = avail;
		avail = avail->link;
	}
	else
		node = (polyPointer*)malloc(sizeof(*node));
	return node;
}
void retNode(polyPointer node)
{
	node->link = avail;
	avail = node;
}
void cerase(polyPointer *ptr)
{
	polyPointer temp;
	if (*ptr) {
		temp = (*ptr)->link;
		(*ptr)->link = avail;
		avail = temp;
		*ptr = NULL;
	}
}
polyPointer cpadd(polyPointer a, polyPointer b)
{
	polyPointer startA, c, lastC;
	int sum, done = FALSE;
	startA = a;
	a = a->link;
	b = b->link;
	c = getNode();
	c->expon = -1; lastC = c;
	do {
		switch (COMPARE(a->expon, b->expon)) {
		case -1:
			attach(b->coef, b->expon, &lastC);
			b = b->link;
			break;
		case 0:
			if (startA == a)
				done = TRUE;
			else {
				sum = a->coef + b->coef;
				if (sum) attach(sum, a->expon, &lastC);
				a = a->link; b = b->link;
			}
			break;
		case 1:
			attach(a->coef, a->expon, &lastC);
			a = a->link;
		}
	} while (!done);
	lastC->link = c;
	return c;
}
int COMPARE(int a, int b)
{
	if (a > b)
		return 1;
	else if (a == b)
		return 0;
	else if (a < b)
		return -1;
}