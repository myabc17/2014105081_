#include <stdio.h>
#include <stdlib.h>

typedef struct polyNode *polyPointer;
typedef struct polyNode {
	int coef;
	int expon;
	polyPointer link;
}polyNode;
polyPointer a, b;

int COMPARE(int a, int b)
{
	if (a > b)
		return 1;
	else if (a == b)
		return 0;
	else if (a < b)
		return -1;
}

polyPointer findLast(polyPointer a)
{
	polyPointer temp = NULL;

	for (; a; a = a->link)
		temp = a;
	
	return temp;
}
void insert(polyPointer *first, polyPointer x, int coefficent, int exponent)
{
	polyPointer temp;
	temp = (polyPointer*)malloc(sizeof(*temp));
	temp->coef = coefficent;
	temp->expon = exponent;
	temp->link = NULL;
	
	if (*first) {
		x->link = temp;
	}
	else {
		*first = temp;
	}
}

void printList(polyPointer *first)
{
	polyPointer a;
	a = *first;
	for (; a; a = a->link) {
		if (a->coef > 0)
			printf("+%dx^%d\t", a->coef, a->expon);
		else if (a->coef < 0)
			printf("%dx^%d\t", a->coef, a->expon);
	}
}

void attach(int coefficient, int exponent, polyPointer *ptr)
{
	polyPointer temp;
	temp = (polyPointer*)malloc(sizeof(*temp));
	temp->coef = coefficient;
	temp->expon = exponent;
	(*ptr)->link = temp;
	*ptr = temp;
}

void erase(polyPointer *ptr)
{
	polyPointer temp;
	while (*ptr) {
		temp = *ptr;
		*ptr = (*ptr)->link;
		free(temp);
	}
}
polyPointer padd(polyPointer a, polyPointer b)
{
	polyPointer c, rear, temp;
	int sum;
	rear = (polyPointer*)malloc(sizeof(*rear));
	c = rear;
	while (a && b)
		switch (COMPARE(a->expon, b->expon)) {
		case -1:
			attach(b->coef, b->expon, &rear);
			b = b->link;
			break;
		case 0:
			sum = a->coef + b->coef;
			if (sum)
				attach(sum, a->expon, &rear);
			a = a->link;
			b = b->link;
			break;
		case 1:
			attach(a->coef, a->expon, &rear);
			a = a->link;
	}
	for (; a; a = a->link)
		attach(a->coef, a->expon, &rear);
	for (; b; b = b->link)
		attach(b->coef, b->expon, &rear);
	rear->link = NULL;
	temp = c;
	c = c->link;
	free(temp);
	return c;
}

void main()
{
	FILE *f1 = fopen("a.txt", "r");
	FILE *f2 = fopen("b.txt", "r");
	polyPointer c = NULL;
	int check, temp[2];//데이터입력값 저장
	a = NULL;
	b = NULL;
	while(1) {
		check = fscanf(f1, "%d %d", &temp[0], &temp[1]);
		if (check == -1)
			break;
		insert(&a, findLast(a), temp[0], temp[1]);
	}
	while (1) {
		check = fscanf(f2, "%d %d", &temp[0], &temp[1]);
		if (check == -1)
			break;
		insert(&b, findLast(b), temp[0], temp[1]);
	}
	c = padd(a, b);
	printf("\ta : ");
	printList(&a);
	printf("\n\tb : ");
	printList(&b);
	printf("\n\ta+b=c : ");
	printList(&c);
	printf("\n");
	erase(&a);
	erase(&b);
	fclose(f1);
	fclose(f2);
}