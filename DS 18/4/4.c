#include <stdio.h>
#define MAX_ELEMENTS 100
#define SWAP(x,y,t) (t=x,x=y,y=t)

typedef struct element{
	int key;
}element;
element list[MAX_ELEMENTS];
element temp;
int count1 = 0;

void quickSort(element a[], int left, int right)
{
	int pivot, i, j;
	count1++;
	if (left < right) {
		i = left; j = right + 1;
		pivot = a[left].key;
		do {
			do i++; while (a[i].key < pivot);
			do j--; while (a[j].key > pivot);
			if (i < j) SWAP(a[i], a[j], temp);
		} while (i < j);
		SWAP(a[left], a[j], temp);
		quickSort(a, left, j - 1);
		quickSort(a, j + 1, right);
	}
}

void main()
{
	FILE *f1 = fopen("input.txt", "r"), *f2 = fopen("output.txt", "w");
	int i, count = 0;
	//list = (element*)malloc(sizeof(*list)*MAX_ELEMENTS);
	while (fscanf(f1, "%d", &list[count].key) != -1) {
		count++;
	}

	quickSort(list, 0, count - 1);
	for (i = 0; i < count; i++) {
		fprintf(f2, "%4d", list[count].key);
		printf("%4d", list[i].key);
	}
	printf("\nquickSort count = %d\n", count1);
	printf("\n");
	fclose(f1);
	fclose(f2);
}