#include <stdio.h>
#define MAX_LIST 100
typedef struct {
	int key;
	char name[20];
	int grade;
}element;
element list[MAX_LIST];

void insert(element e, element a[], int i)
{
	a[0] = e;
	while (e.key < a[i].key)
	{
		a[i + 1] = a[i];
		i--;;
	}
	a[i + 1] = e;
}

void insertionSort(element a[], int n)
{
	int j;
	for (j = 2; j <= n; j++) {
		element temp = a[j];
		insert(temp, a, j - 1);
	}
}

void main()
{
	FILE *f1 = fopen("input.txt", "r"), *f2 = fopen("output.txt","w");
	
	int i, elementsize;
	fscanf(f1, "%d", &elementsize);
	for (i = 1; i <= elementsize; i++) {
		fscanf(f1, "%d %s %d", &list[i].key, list[i].name, &list[i].grade);
		//printf("%d %s %d\n", list[i].key, list[i].name, list[i].grade);
	}
	printf("\n");
	insertionSort(list, elementsize);
	for (i = 1; i <= elementsize; i++) {
		fprintf(f2, "%d %s %d\n", list[i].key, list[i].name, list[i].grade);
		printf("%d %s %d\n", list[i].key, list[i].name, list[i].grade);
	}
	fclose(f1);
	fclose(f2);
}