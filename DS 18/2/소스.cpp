#include <stdio.h>
#include <stdlib.h>
#define MAX_ELEMENT 20
#define SWAP(x,y,t) (t=x,x=y,y=t)
typedef struct element{
	int key;
};
element a[MAX_ELEMENT];

int quickSort(element a[], int left, int right);
int count = 0;
void main()
{
	int i, num,limit;

	FILE *fp = fopen("input.txt", "r");
	FILE *fc = fopen("output.txt", "w");

	i = 0;
	while (fscanf(fp, "%d", &num) != EOF)
	{
		a[i].key = num;
		i++;
	}

	limit = i-1;
	count=quickSort(a, 0, limit);

	for (i = 0; i <=limit; i++)
	{
		printf("%d\n", a[i].key);
		fprintf(fc, "%d\n", a[i].key);
	}
	printf("count : %d\n", count);
	fclose(fp);
	fclose(fc);
}

int quickSort(element a[], int left, int right)
{
	int pivot, i, j;
	
	count++;
	element temp;
	if (left < right)
	{
		i = left; j = right + 1;
		pivot = a[left].key;
		do{
			do i++; while (a[i].key < pivot);
			do j--; while (a[j].key > pivot);
			if (i < j) SWAP(a[i], a[j], temp);
		} while (i < j);
		SWAP(a[left], a[j], temp);
		
		quickSort(a, left, j - 1); 
		quickSort(a, j + 1, right); 
		
	}
	return count;
}
