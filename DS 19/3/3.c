#include <stdio.h>
#define MAX_SIZE 20
#define SWAP(x,y,t) (t=x, x=y, y=t)
typedef struct element {
	int key;
}element;

element a[MAX_SIZE];
FILE *f1, *f2;

void adjust(element a[], int root, int n)
{
	int child, rootkey;
	element temp;
	temp = a[root];
	rootkey = a[root].key;
	child = 2 * root;
	while (child <= n) {
		if ((child < n) && (a[child].key < a[child + 1].key))
			child++;
		if (rootkey > a[child].key)
			break;
		else {
			a[child / 2] = a[child];
			child *= 2;
		}
	}
	a[child / 2] = temp;
}
void print(element a[], int size)
{
	int j;
	for (j = 1; j <= size; j++)
		printf("%-3d", a[j]);
	printf("\n");
}
void heapSort(element a[], int n)
{
	int i, j;
	element temp;
	for (i = n / 2; i > 0; i--) {
		adjust(a, i, n);
		print(a, n);
	}
	for (i = n - 1; i > 0; i--) {
		SWAP(a[1], a[i + 1], temp);
		adjust(a, 1, i);
		print(a, n);
	}
}

void main()
{
	f1 = fopen("input.txt", "r");
	f2 = fopen("output.txt", "w");
	int i, temp=0, size = 1;
	while (fscanf(f1, "%d", &temp) != EOF) {
		a[size].key = temp;
		size += 1;
	}
	heapSort(a, size - 1);
	for (i = 1; i < size; i++) {
		fprintf(f2, "%-3d", a[i]);
	}
	fclose(f1);
	fclose(f2);
}