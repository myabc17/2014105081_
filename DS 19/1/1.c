#include <stdio.h>
#define MAX_SIZE 20
typedef struct element {
	int key;
}element;
int s;
element initList[MAX_SIZE], mergedList[MAX_SIZE];

void merge(element initList[], element mergedList[], int i, int m, int n)
{
	int j, k, t;
	j = m + 1;
	k = i;

	while (i <= m && j <= n) {
		if (initList[i].key <= initList[j].key)
			mergedList[k++] = initList[i++];
		else
			mergedList[k++] = initList[j++];
	}
	if (i>m)
	for (t = j; t <= n; t++)
		mergedList[t] = initList[t];
	else
	for (t = i; t <= m; t++)
		mergedList[k + t - i] = initList[t];
}

void mergePass(element initList[], element mergedList[], int n, int s)
{
	int i, j;;
	for (i = 1; i <= n - 2 * s + 1; i += 2 * s)
		merge(initList, mergedList, i, i + s - 1, i + 2 * s - 1);
	if (i + s - 1 < n)
		merge(initList, mergedList, i, i + s - 1, n);
	else
	for (j = i; j <= n; j++)
		mergedList[j] = initList[j];
}
void print(element a[], element extra[], int n, int s)
{
	int i;
	printf("\nsize of segment : %d\na     : ", s);
	for (i = 1; i <= n; i++) {
		printf("%4d", a[i].key);
	}
	printf("\nextra : ");
	for (i = 1; i <= n; i++) {
		printf("%4d", extra[i].key);
	}
	printf("\n");
}
void mergeSort(element a[], int n)
{
	element extra[MAX_SIZE];
	while (s < n) {
		mergePass(a, extra, n, s);
		print(a, extra, n, s);
		s *= 2;
		mergePass(extra, a, n, s);
		print(a, extra, n, s);
		s *= 2;
	}
}

void main()
{
	FILE *f1 = fopen("input.txt", "r"), *f2 = fopen("output.txt", "w");

	int i, input, size = 1;
	s = 1;
	while (fscanf(f1, "%d", &input) != EOF) {
		initList[size].key = input;
		printf("%3d", initList[size]);
		size++;
	}
	printf("\n");
	mergeSort(initList, size - 1);
	for (i = 1; i < size; i++)
		fprintf(f2, "%-3d", initList[i]);
	fclose(f1);
	fclose(f2);
}