#include <stdio.h>
#define MAX_SIZE 20
typedef struct element {
	int key;
}element;
FILE *f1, *f2;
element a[MAX_SIZE];

int listMerge(element a[], int link[], int start1, int start2)
{
	int last1, last2, lastResult = 0;
	for (last1 = start1, last2 = start2; last1&&last2;) {
		if (a[last1].key <= a[last2].key) {
			link[lastResult] = last1;
			lastResult = last1; last1 = link[last1];
		}
		else {
			link[lastResult] = last2;
			lastResult = last2; last2 = link[last2];
		}
	}
	if (last1 == 0)
		link[lastResult] = last2;
	else
		link[lastResult] = last1;
	return link[0];
}
int rmergeSort(element a[], int link[], int left, int right)
{
	if (left >= right) return left;
	int mid = (left + right) / 2;
	return listMerge(a, link, rmergeSort(a, link, left, mid), rmergeSort(a, link, mid + 1, right));
}
void print(element a[], int link[], int size)
{
	int i, j = link[0];
	for (i = 0; i < size; i++) {
		printf("%-4d", a[j]);
		fprintf(f2, "%-4d", a[j]);
		j = link[j];
	}
	printf("\n");
}
void main()
{
	int temp, size = 1;
	int link[MAX_SIZE];
	f1 = fopen("input.txt", "r");
	f2 = fopen("output.txt", "w");
	while (fscanf(f1, "%d", &temp) != EOF) {
		a[size].key = temp;
		link[size] = 0;
		size++;
	}
	temp = rmergeSort(a, link, 1, size - 1);
	print(a, link, size - 1);
	fclose(f1);
	fclose(f2);
}