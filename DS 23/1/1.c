#include <stdio.h>
#include <stdlib.h>
#define SWAP(x,y,t) (t=x, x=y, y=t)
#define MAX_SIZE 10

typedef struct {
	int key;
}element;
typedef struct leftist *leftistTree;
typedef struct leftist {
	leftistTree leftChild;
	element data;
	leftistTree rightChild;
	int shortest;
};

leftistTree queue[MAX_SIZE];
int front = -1;
int rear = -1;
FILE *f;

void minMeld(leftistTree *a, leftistTree *b);
void minUnion(leftistTree *a, leftistTree *b);
void insert(leftistTree *a);
void inorder(leftistTree ptr);
void initialize();
void delete(leftistTree ptr);
leftistTree deleteq();

void main(void)
{
	f = fopen("input.txt", "r");
	int input;
	leftistTree temp;
	while (fscanf(f, "%d", &input) != EOF) {
		temp = (leftistTree)malloc(sizeof(*temp));
		temp->data.key = input;
		temp->shortest = 1;
		temp->leftChild = NULL; temp->rightChild = NULL;

		insert(temp);
	}

	while (1) {
		initialize();
		if (front == rear - 1)
			break;
	}
	printf("After initialize : \n");
	inorder(queue[rear]);

	printf("\n\nAfter delete Min key\n");
	delete(queue[rear]);
	inorder(queue[rear]);

	printf("\n\nEnter the key : ");
	scanf("%d", &input);
	temp = (leftistTree)malloc(sizeof(*temp));
	temp->data.key = input;
	temp->shortest = 1;
	temp->leftChild = NULL; temp->rightChild = NULL;
	insert(temp);
	initialize();
	printf("\nAfter enter the key\n");
	inorder(queue[rear]);
	printf("\n");
	fclose(f);
}
void delete(leftistTree ptr)
{
	leftistTree a, b;

	a = ptr->leftChild;
	b = ptr->rightChild;

	if (a == NULL)
		queue[rear] = b;
	else if (b == NULL)
		queue[rear] = a;
	else
	{
		queue[rear] = a;
		queue[++rear] = b;
		initialize();
	}
}
leftistTree deleteq()
{
	leftistTree temp;
	front = ++front % MAX_SIZE;

	temp = (leftistTree)malloc(sizeof(*temp));
	temp->data.key = queue[front]->data.key;
	temp->leftChild = queue[front]->leftChild;
	temp->rightChild = queue[front]->rightChild;
	temp->shortest = queue[front]->shortest;

	free(queue[front]);

	return temp;
}
void initialize()
{
	leftistTree tempA, tempB;
	int i;

	tempA = deleteq();
	tempB = deleteq();
	minMeld(&tempA, &tempB);
	insert(tempA);

}
void inorder(leftistTree ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%3d", ptr->data.key);
		inorder(ptr->rightChild);
	}
}
void insert(leftistTree *a)
{
	rear = ++rear % MAX_SIZE;
	queue[rear] = a;
}
void minMeld(leftistTree *a, leftistTree *b)
{
	if (!*a)
		*a = *b;
	else if (*b)
		minUnion(a, b);
	*b = NULL;
}
void minUnion(leftistTree *a, leftistTree *b)
{
	leftistTree temp;
	if ((*a)->data.key > (*b)->data.key)
		SWAP(*a, *b, temp);
	if (!(*a)->rightChild)
		(*a)->rightChild = *b;
	else
		minUnion(&(*a)->rightChild, b);

	if (!(*a)->leftChild) {
		(*a)->leftChild = (*a)->rightChild;
		(*a)->rightChild = NULL;
	}
	else if ((*a)->leftChild->shortest <
		(*a)->rightChild->shortest)
		SWAP((*a)->leftChild, (*a)->rightChild, temp);
	(*a)->shortest = (!(*a)->rightChild) ? 1 : (*a)->rightChild->shortest + 1;
}