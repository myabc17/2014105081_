#include <stdio.h>
#include <stdlib.h>
#define MAX_STACKS_SIZE 100
#define MAX_QUEUE_SIZE 100

typedef enum { lparen, rparen, plus, minus, times, divide, mod, eos, operand } precedence;
typedef struct node *treePointer;
typedef struct node {
	char data;//�������
	treePointer leftChild, rightChild;
}node;
treePointer root;
treePointer stack[MAX_STACKS_SIZE];
char expr[81];//postfix expression
int top = -1;
treePointer queue[MAX_QUEUE_SIZE];
int front = 0, rear = 0;

void addq(treePointer item);
treePointer deleteq();
void inorder(treePointer ptr);
void iterInorder(treePointer node);
void levelOrder(treePointer ptr);
treePointer eval(void);
precedence getToken(char *symbol, int *n);
void push(char item);
treePointer pop(void);
void queueFull();
void main()
{
	FILE *f = fopen("input.txt", "r");
	treePointer temp, temp2;
	treePointer a;
	int i = 0;
	temp = (treePointer)calloc(1, sizeof(*temp));
	root = (treePointer)calloc(1, sizeof(*root));


	printf("the length of input string should be less than 80\ninput string <postfix expression> : ");
	while (1) {
		if (fscanf(f, "%c", &expr[i]) == -1)
			break;
		i++;
	}

	printf("%s", expr);
	printf("\ncreating its binary tree\n\n");
	root = eval();

	printf("iterative inorder traversal\t:");
	iterInorder(root);
	printf("level order traversal\t:");
	levelOrder(root);
}

void queueFull()
{
	printf("Queue is Full\n");
	rear = rear - 1;
}
treePointer pop(void)
{
	return stack[top--];
}
void push(treePointer item)
{
	stack[++top] = item;
}

treePointer queueEmpty()
{
	return NULL;
}
treePointer deleteq()
{
	treePointer item;
	if (front == rear)
		return queueEmpty();
	front = (front + 1) % MAX_QUEUE_SIZE;
	return queue[front];
}
void addq(treePointer item)
{
	rear = (rear + 1) % MAX_QUEUE_SIZE;
	if (front == rear)
		queueFull();
	queue[rear] = item;
}
void iterInorder(treePointer node)
{
	top = -1;
	for (;;) {
		for (; node; node = node->leftChild)
			push(node);
		node = pop();
		if (!node) break;
		printf("%c", node->data);
		node = node->rightChild;
	}
	printf("\n");
}
void levelOrder(treePointer ptr)
{
	int front = 0, rear = 0;
	treePointer queue[MAX_QUEUE_SIZE];
	if (!ptr) return;
	addq(ptr);
	for (;;) {
		ptr = deleteq();
		if (ptr) {
			printf("%c", ptr->data);
			if (ptr->leftChild)
				addq(ptr->leftChild);
			if (ptr->rightChild)
				addq(ptr->rightChild);
		}
		else break;
	}
	printf("\n");
}

precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '(': return lparen;
	case ')': return rparen;
	case '+': return plus;
	case '-': return minus;
	case '/': return divide;
	case '*': return times;
	case '%': return mod;
	case '\0': return eos;
	default: return operand;
	}
}
treePointer eval(void)
{
	precedence token;
	treePointer temp, temp2;
	temp = (treePointer)calloc(1, sizeof(*temp));
	char symbol;
	int n = 0;
	top = -1;
	token = getToken(&symbol, &n);
	while (token != eos) {
		if (token == operand) {
			temp2 = (treePointer)calloc(1, sizeof(*temp2));
			temp2->data = symbol;
			temp2->leftChild = NULL;
			temp2->rightChild = NULL;
			push(temp2);
		}
		else {
			temp = (treePointer)calloc(1, sizeof(*temp));
			temp->rightChild = pop();
			temp->leftChild = pop();
			switch (token) {
			case plus:
				temp->data = '+';
				push(temp);
				break;
			case minus:
				temp->data = '-';
				push(temp);
				break;
			case times:
				temp->data = '*';
				push(temp);
				break;
			case divide:
				temp->data = '/';
				push(temp);
				break;
			case mod:
				temp->data = '%';
				push(temp);
				break;
			}
		}
		token = getToken(&symbol, &n);
	}
	return stack[0];
}

void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%c", ptr->data);
		inorder(ptr->rightChild);
	}
}