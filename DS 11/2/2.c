#include <stdio.h>
#include <stdlib.h>
#define MAX_STACKS 100

typedef enum { not, and ,or, eos, operand} precedence;
typedef struct node *treePointer;
typedef struct node {
	char c;//�������
	treePointer leftChild, rightChild;
}node;
treePointer root;
treePointer stack[MAX_STACKS];
char expr[81];//postfix expression
int top = -1;

void inorder(treePointer ptr);
void preorder(treePointer ptr);
void postorder(treePointer ptr);
treePointer eval(void);
precedence getToken(char *symbol, int *n);
void push(char item);
treePointer pop(void);

void main()
{
	FILE *f = fopen("input.txt", "r");
	treePointer temp, temp2;
	treePointer a;
	int i = 0;
	temp = (treePointer)calloc(1, sizeof(*temp));
	root = (treePointer)calloc(1, sizeof(*root));


	printf("the length of input string should be less than 80\ninput string <postfix expression> : ");
	while (1) {
		if (fscanf(f, "%c", &expr[i]) == -1)
			break;
		i++;
	}

	printf("%s", expr);
	printf("\ncreating its binary tree\n\n");
	root = eval();

	printf("inorder traversal\t:");
	inorder(root);
}


treePointer pop(void)
{
	return stack[top--];
}
void push(treePointer item)
{
	stack[++top] = item;
}
precedence getToken(char *symbol, int *n)
{
	*symbol = expr[(*n)++];
	switch (*symbol) {
	case '+': return and;
	case '-': return or;
	case '/': return not;
	case '\0': return eos;
	default: return operand;
	}
}
treePointer eval(void)
{
	precedence token;
	treePointer temp, temp2;
	temp = (treePointer)calloc(1, sizeof(*temp));
	char symbol;
	int n = 0;
	top = -1;
	token = getToken(&symbol, &n);
	while (token != eos) {
		if (token == operand) {
			temp2 = (treePointer)calloc(1, sizeof(*temp2));
			temp2->c = symbol;
			temp2->leftChild = NULL;
			temp2->rightChild = NULL;
			push(temp2);
		}
		else if (token == not)
		{
			temp = (treePointer)calloc(1, sizeof(*temp));
			temp->rightChild = pop();
			temp->c = '/';
			temp->leftChild = NULL;
			push(temp);
		}
		else {
			temp = (treePointer)calloc(1, sizeof(*temp));
			temp->rightChild = pop();
			temp->leftChild = pop();
			switch (token) {
			case and:
				temp->c = '+';
				push(temp);
				break;
			case or:
				temp->c = '-';
				push(temp);
				break;
			}
		}
		token = getToken(&symbol, &n);
	}
	return stack[0];
}
void postorder(treePointer ptr)
{
	if (ptr) {
		postorder(ptr->leftChild);
		postorder(ptr->rightChild);
		printf("%c", ptr->c);
	}
}

void preorder(treePointer ptr)
{
	if (ptr) {
		printf("%c", ptr->c);
		preorder(ptr->leftChild);
		preorder(ptr->rightChild);
	}
}

void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%c", ptr->c);
		inorder(ptr->rightChild);
	}
}