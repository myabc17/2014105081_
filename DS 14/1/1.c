#include <stdio.h>
#include <stdlib.h>
#define MAX_VERTEX 10
typedef struct node *nodePointer;
typedef struct node {
	int data;
	nodePointer link;
}node;
nodePointer head[MAX_VERTEX];

void push(nodePointer *ptr, int vertex, int edge)
{
	nodePointer temp1, temp2;
	temp1 = (nodePointer)calloc(1, sizeof(*temp1));
	temp2 = (nodePointer)calloc(1, sizeof(*temp2));
	temp1->link = NULL;
	temp2->link = NULL;
	if (ptr[vertex] == NULL) {
		temp1->data = edge;
		temp1->link = NULL;
		ptr[vertex]->link = temp1;
	}
	else {
		temp1->data = edge;
		temp1->link = ptr[vertex]->link;
		ptr[vertex]->link = temp1;
	}
	if (ptr[edge]->link == NULL) {
		temp2->data = vertex;
		temp2->link = NULL;
		ptr[edge]->link = temp2;
	}
	else {
		temp2->data = vertex;
		temp2->link = ptr[edge]->link;
		ptr[edge]->link = temp2;
	}
}
void main()
{
	int i = 0, j = 0, V, E, vertex, edge;
	nodePointer temp;
	nodePointer *adjLists;
	
	FILE *f = fopen("input.txt", "r");
	fscanf(f, "%d %d", &vertex, &edge);
	adjLists = (nodePointer*)calloc(vertex, sizeof(*adjLists));
	for (i = 0; i < vertex; i++)
		head[i] = (nodePointer)calloc(1, sizeof(*head[i]));
	i = 0;
	while (i != edge) {
		fscanf(f, "%d %d", &V, &E);
		push(head, V, E);
		i++;
	}
	printf("          < graph - adjacency list representation >\n");
	for (i = 0; i < vertex; i++) {
		printf("    egdes incident to vertex %d : ", i);
		for (j = 0; j < vertex; j++) {
			head[i] = head[i]->link;
			if (head[i] == NULL)
				break;
			printf(" (%2d, %2d) ", i, head[i]->data);
		}
		printf("\n");
	}
	printf("\n");
}