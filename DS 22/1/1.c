#include <stdio.h>
#define MAX_SIZE 11

typedef struct element{
	unsigned int key;
}element;
element *ht[MAX_SIZE] = { NULL };
FILE *f;
int b = 8, s = 1, numberofcomp = 1;
int randNum[MAX_SIZE];

int h(int k)
{
	return k%b;
}
element* search(int k)
{
	numberofcomp = 1;
	int homeBucket, currentBucket;
	homeBucket = h(k);
	for (currentBucket = homeBucket; ht[currentBucket] && ht[currentBucket]->key != k;) {
		currentBucket = (homeBucket + randNum[numberofcomp]) % b;
		numberofcomp++;
		if (currentBucket == homeBucket)
			return NULL;
	}
	if(ht[currentBucket])
		if (ht[currentBucket]->key == k)
			return ht[currentBucket];
	return NULL;
}
void insert(element *d)
{
	int i, input, k, inputtemp;
	element *temp, *check;
	input = h(d->key);
	inputtemp = input;
	if (!ht[input])
		ht[input] = d;
	else {
		i = 1;
		while (1) {
			input = (inputtemp + randNum[i]) % b;
			if (!ht[input]) {
				ht[input] = d;
				break;
			}
			else {
				i++;
				continue;
			}
		}
	}
}
void main()
{
	f = fopen("input.txt", "r");
	element *temp;
	int seed, i = 0, j, temp2, ch;
	int store[MAX_SIZE];
	printf("key sequence from file : ");
	while (fscanf(f, "%d", &temp2) != -1) {
		store[i] = temp2;
		printf("%4d", store[i]);
		i++;
	}

	printf("\ninput seed >> ");
	scanf("%d", &seed);
	printf("\n");
	srand(seed);
	
	for (i = 1; i < b; i++) {
		randNum[i] = rand() % b + 1;
		if (randNum[i] >= 8) {
			i = i - 1;
			continue;
		}
		for (j = 1; j < i; j++) {
			if (randNum[j] == randNum[i]) {
				i = i - 1;
				break;
			}
		}
	}
	for (i = 1; i < b; i++)
		printf("randNum[%d] : %d\n", i, randNum[i]);
	for (i = 0; i < b; i++) {
		temp = (element*)malloc(sizeof(*temp));
		temp->key = store[i];
		insert(temp);
	}
	

	printf("%9skey\n", "");
	for (i = 0; i < b; i++) {
		if (ht[i])
			printf("ht[%2d] : %4d\n", i, ht[i]->key);
		else
			printf("ht[%2d] : %4s\n", i, "");
	}

	while (1) {
		printf("input 0 to quit\n");
		printf("key to search >> ");
		scanf("%d", &ch);
		if (ch == 0)
			break;
		temp = search(ch);
		if (temp)
			printf("key : %d, the number of comparisions : %d\n\n", temp->key, numberofcomp);
		else
			printf("it dosen't exist!\n\n");
	}
}