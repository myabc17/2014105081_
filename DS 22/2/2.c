#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 11
#define STRING_MAX 11
#define BUCKET_SIZE 11

typedef struct element {
	char item[20];
	int key;
}element;
typedef struct node nodePointer;
typedef struct node {
	element data;
	nodePointer *link;
}node;

node *ht[MAX_SIZE] = { NULL };
int numberofcomp;
FILE *f;
int b = 11, s = 1;
unsigned int stringToInt(char *key)
{
	int number = 0;
	while (*key)
		number += *key++;
	return number;
}
int h(int k)
{
	return k%b;
}
node* search(int k)
{
	numberofcomp = 1;
	node *current;
	int homeBucket = h(k);
	for (current = ht[homeBucket]; current; current = current->link) {
		if (current->data.key == k) 
			return current;
		numberofcomp++;
	}
	
	return NULL;
}
void insert(node *d, int size)
{
	int i, input, k;
	node *temp, *check;
	input = h(d->data.key);
	d->link = NULL;
	if (!ht[input]) {
		ht[input] = d;
	}
	else {
		for (temp = ht[input]; temp->link; temp = temp->link);
		temp->link = d;
		}
}


void main()
{
	f = fopen("input.txt", "r");
	char temp[20];
	int i, j, count = 0, check = 0;
	node *print, *in, *out, *input;
	printf("input strings : ");
	while (fscanf(f, "%s", temp) != EOF) {
		printf("%s ", temp);
		in = (node *)malloc(sizeof(node));
		in->data.key = stringToInt(temp);
		strcpy(in->data.item, temp);
		insert(in, count);
		count++;
	}
	printf("\n\n%20s%7s\n", "item", "key");
	for (i = 0; i <= count; i++) {
		printf("ht[%2d] : ", i);
		for (print = ht[i]; print; print = print->link) {
			printf(" <%s %d> ", print->data.item, print->data.key);
		}
		printf("\n");
	}

	while (1) {
		printf("input ^Z to quit\n");
		printf("starting to search >> ");
		if (scanf("%s", temp) != -1) {
			print = search(stringToInt(temp));
			if (print)
				printf("item : %s, key : %d, the number of comparisions : %d\n\n", print->data.item, print->data.key, numberofcomp);
			else
				printf("it doesn't exist!\n\n");
		}
		else
			break;
	}
}