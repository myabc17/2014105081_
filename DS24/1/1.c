#include <stdio.h>
#define TRUE 1
#define FALSE 0

typedef struct {
	char key[81];
} element;
typedef struct treeNode *treePointer;
typedef struct treeNode {
	treePointer leftChild;
	element data;
	short int bf;
	treePointer rightChild;
}treeNode;
FILE *f;
treePointer root;


void avlInsert(treePointer *parent, element x, int *unbalanced);
void leftRotation(treePointer *parent, int *unbalanced);
void rightRotation(treePointer *parent, int *unbalanced);
void inorder(treePointer ptr);


void main()
{
	f = fopen("input.txt", "r");
	int n = FALSE;
	element temp;
	while (fscanf(f, "%s", temp.key) != EOF) {
		avlInsert(&root, temp, &n);
	}
	inorder(root);
	printf("\n");
}


void inorder(treePointer ptr)
{
	if (ptr) {
		inorder(ptr->leftChild);
		printf("%s ", ptr->data.key);
		inorder(ptr->rightChild);
	}
}
void avlInsert(treePointer *parent, element x, int *unbalanced)
{
	if (!*parent) {
		*unbalanced = TRUE;
		*parent = (treePointer)malloc(sizeof(treeNode));
		(*parent)->leftChild = (*parent)->rightChild = NULL;
		(*parent)->bf = 0; (*parent)->data = x;
	}
	else if (strcmp(x.key, (*parent)->data.key) < 0) {
		avlInsert(&(*parent)->leftChild, x, unbalanced);
		if (*unbalanced)
			switch ((*parent)->bf) {
			case -1: (*parent)->bf = 0;
				*unbalanced = FALSE; break;
			case 0: (*parent)->bf = 1; break;
			case 1: leftRotation(parent, unbalanced);
		}
	}
	else if (strcmp(x.key, (*parent)->data.key) > 0) {
		avlInsert(&(*parent)->rightChild, x, unbalanced);
		if (*unbalanced)
			switch ((*parent)->bf) {
			case 1: (*parent)->bf = 0;
				*unbalanced = FALSE; break;
			case 0: (*parent)->bf = -1; break;
			case -1: rightRotation(parent, unbalanced);
		}
	}
	else {
		*unbalanced = FALSE;
		printf("The key is already in the tree\n");
	}
}
void rightRotation(treePointer *parent, int *unbalanced)
{
	treePointer grandChild, child;
	child = (*parent)->rightChild;
	if (child->bf == -1) {
		(*parent)->rightChild = child->leftChild;
		child->leftChild = *parent;
		(*parent)->bf = 0;
		(*parent) = child;
	}
	else {
		grandChild = child->leftChild;
		child->leftChild = grandChild->rightChild;
		grandChild->rightChild = child;
		(*parent)->rightChild = grandChild->leftChild;
		grandChild->leftChild = *parent;
		switch (grandChild->bf) {
		case -1: (*parent)->bf = 1;
			child->bf = 0;
			break;
		case 0: (*parent)->bf = child->bf = 0;
			break;
		case 1: (*parent)->bf = 0;
			child->bf = -1;
		}
		*parent = grandChild;
	}
	(*parent)->bf = 0;
	*unbalanced = FALSE;
}
void leftRotation(treePointer *parent, int *unbalanced)
{
	treePointer grandChild, child;
	child = (*parent)->leftChild;
	if (child->bf == 1) {
		(*parent)->leftChild = child->rightChild;
		child->rightChild = *parent;
		(*parent)->bf = 0;
		(*parent) = child;
	}
	else {
		grandChild = child->rightChild;
		child->rightChild = grandChild->leftChild;
		grandChild->leftChild = child;
		(*parent)->leftChild = grandChild->rightChild;
		grandChild->rightChild = *parent;
		switch (grandChild->bf) {
		case 1: (*parent)->bf = -1;
			child->bf = 0;
			break;
		case 0: (*parent)->bf = child->bf = 0;
			break;
		case -1: (*parent)->bf = 0;
			child->bf = 1;
		}
		*parent = grandChild;
	}
	(*parent)->bf = 0;
	*unbalanced = FALSE;
}