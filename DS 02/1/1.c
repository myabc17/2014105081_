#include <stdio.h>
#include <time.h>
#include "selectionSort.h"
#define MAX_SIZE 1001

void main(void)
{
	int i, n, step = 10;
	int a[MAX_SIZE];
	double duration;
	clock_t start;

	printf("	n	time\n");
	for (n = 0; n <= 1000; n += step)
	{
		for (i = 0; i < n; i++)
		{
			a[i] = n - i;
		}
		start = clock();
		sort(a, n);
		duration = ((double)(clock() - start))/CLOCKS_PER_SEC;
		printf("%6d	   %f\n", n, duration);
		if (n == 100)
		{
			step = 100;
		}
	}
}
//2014105081 전우혁
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.