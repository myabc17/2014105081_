#include <stdio.h>
#include "selectionsort3-2.h"
void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void sort(int list[], int n)
{
	int i, j, min1;
	for (i = 0; i < n - 1; i++)
	{
		min1 = i;
		for (j = i + 1; j < n; j++)
		{
			if (list[j] < list[min1])
			{
				min1 = j;
			}
		}swap(&list[i], &list[min1]);
	}
}
//2014105081 전우혁
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.