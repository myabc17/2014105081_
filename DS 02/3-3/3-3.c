#include <stdio.h>
#include <time.h>
#include "selectionsort3-3.h"
#define MAX_SIZE 1001

void main(void)
{
	int i, n, step = 10;
	int a[MAX_SIZE];
	double duration;
	srand(time(0));
	printf("	n	repetitions		time\n");
	for (n = 0; n <= 1000; n += step)
	{
		long repetitions = 0;
		clock_t start = clock();
		do
		{
			repetitions++;
			for (i = 0; i < n; i++)
				a[i] = rand();
			sort(a, n);
		} while (clock() - start < 1000);
		duration = ((double)(clock() - start)) / CLOCKS_PER_SEC;
		duration /= repetitions;
		printf("%6d		%9d		%f\n", n, repetitions, duration);
		if (n == 100)
			step = 100;
	}
}
//2014105081 전우혁
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.