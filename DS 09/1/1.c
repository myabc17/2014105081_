#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 24
#define FALSE 0
#define TRUE 1

typedef struct node *nodePointer;
typedef struct node {
	int data;
	nodePointer link;
}node;

void main()
{
	short int out[MAX_SIZE];
	short int store[MAX_SIZE][2];
	nodePointer seq[MAX_SIZE];
	nodePointer x, y, top;
	int i, j, n, count = 0, a = 0;
	FILE *f = fopen("input.txt", "r");

	printf("/* MAX_SIZE of a set S : 24 */\n");

	fscanf(f, "%d", &n);
	printf("current size of S : %d\n", n);
	for (i = 0; i < n; i++) {
		out[i] = FALSE; seq[i] = NULL;
	}

	
	// push() ����
	//printf("Enter a pair of numbers (-1 -1 to quit) : ");
	fscanf(f, "%d%d", &i, &j);
	store[0][0] = i; store[0][1] = j;
	count = 1;
	while (count != n) {
		a += 1;
		count += 1;
		x = (nodePointer*)malloc(sizeof(*x));
		x->data = j; x->link = seq[i]; seq[i] = x;
		x = (nodePointer*)malloc(sizeof(*x));
		x->data = i; x->link = seq[j]; seq[j] = x;
		//printf("Enter a pair of numbers (-1 -1 to quit) : ");
		fscanf(f, "%d%d", &i, &j);
		store[a][0] = i; store[a][1] = j;
	}
	printf("S = { ");
	for (i = 0; i < n; i++) {
		printf("%d, ", i);
	}
	printf("}\ninput pairs : ");
	for (i = 0; i < n; i++) {
		printf("%dR%d ", store[i][0], store[i][1]);
	}
	printf("\n");
	// pop() ����
	for (i = 0; i < n;i++)
	if (out[i] == FALSE) {
		printf("\nnew class: %5d", i);
		out[i] = TRUE;
		x = seq[i]; top = NULL;
		for (;;) {
			while (x != NULL) {
				j = x->data;
				if (out[j] == FALSE) {
					printf("%5d", j); out[j] = TRUE;
					y = x->link; x->link = top; top = x; x = y;
				}
				else x = x->link;
			}
			if (!top) break;
			x = seq[top->data]; top = top->link;
		}
	}
	printf("\n");
	fclose(f);
}