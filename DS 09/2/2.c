#include <stdio.h>
#include <stdlib.h>

typedef struct node *nodePointer;
typedef struct node {
	nodePointer llink;
	int data;
	nodePointer rlink;
}node;
nodePointer head = NULL;

void dinsert(nodePointer node, nodePointer newnode);
void ddelete(nodePointer node, nodePointer deleted);
void forwardprint(nodePointer node, int size);
void backwardprint(nodePointer node, int size);

void main()
{
	FILE *f = fopen("input.txt", "r");
	nodePointer newnode;
	nodePointer node;
	nodePointer temp;
	
	int n, count = 0, count2 = 0, count3 = 0, check;
	head = (nodePointer*)malloc(sizeof(*head));
	temp = (nodePointer*)malloc(sizeof(*head));
	head->llink = head;
	head->rlink = head;
	
	node = (nodePointer*)malloc(sizeof(*node));
	printf("After creating a doubly linked circular list with a head node : \n");
	
	while ((check = fscanf(f, "%d", &n) != -1)) {
		count += 1;
		newnode = (nodePointer*)malloc(sizeof(*newnode));
		newnode->data = n;
		dinsert(head->llink, newnode);
	}
	count2 = count;
	forwardprint(head, count);
	backwardprint(head, count);
	count = 0;
	printf("After deleting numbers less than and equal to 50 :\n");
	
	while (1) {
		temp = head;
		while (1) {
			temp = temp->rlink;
			if (temp->data <= 50) {
				ddelete(head, temp);
				count2--;
				break;
			}
			else if (temp->data == head->data)
				break;
		}
		if (temp->data == head->data)
			break;
		
	}
	forwardprint(head, count2 + 1);
	backwardprint(head, count2 + 1);
	count3 = count2 + 1;
	while (1) {
		temp = head;
		temp = temp->rlink;
		if (temp->data == head->data)
			break;
		ddelete(head, temp);
		count3--;
	}
	printf("\nAfter deleting all nodes except for the header node : \n");
	forwardprint(head, count3);
	backwardprint(head, count3);
	fclose(f);
}



void ddelete(nodePointer node, nodePointer deleted)
{
	if (node == deleted)
		printf("Deletion of header node not permitted.\n");
	else {
		deleted->llink->rlink = deleted->rlink;
		deleted->rlink->llink = deleted->llink;
		free(deleted);
	}
}
void dinsert(nodePointer node, nodePointer newnode)
{
	newnode->llink = node;
	newnode->rlink = node->rlink;
	node->rlink->llink = newnode;
	node->rlink = newnode;
}
void backwardprint(nodePointer node, int size)
{
	int count = 0;
	printf("\nbackward\n");
	while (count < size)
	{
		node = node->llink;
		printf("%2d ", node->data);
		count++;
		if (count % 10 == 0)
			printf("\n");
	}
	printf("\n");
}
void forwardprint(nodePointer node, int size)
{
	int count = 0;
	printf("\nforward\n");
	while (count < size)
	{
		node = node->rlink;
		printf("%2d ", node->data);
		count++;
		if (count % 10 == 0)
			printf("\n");	
	}
	printf("\n");
}